import { IPlantTitles } from './IPlantTitles'

export interface IPlantCollection extends IPlantTitles {
	id: string
	quantity: number
}
