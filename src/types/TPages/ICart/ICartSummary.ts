import { ICartState } from '@/types/TStore/ICartState'

export interface ICartSummary {
	cart: ICartState
}
