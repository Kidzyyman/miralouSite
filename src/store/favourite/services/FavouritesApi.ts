import { toast } from 'react-toastify'

import { AnyAction, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

import { AppState } from '@/store'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'
import { IFavouriteState } from '@/types/TStore/IFavouriteState'

export const fetchFavourites: any = createAsyncThunk<
	IFavouriteState[],
	undefined,
	{ rejectValue: string }
>('favourite/fetchFavourites', async function (_, { rejectWithValue }) {
	const responce = await axios.get(
		'https://63c325198bb1ca34755e00d7.mockapi.io/products'
	)

	if (responce.statusText !== 'OK') {
		return rejectWithValue('Server Error!')
	}
	const data = await responce.data

	return data
})

export const fetchAddFavourite: any = createAsyncThunk<
	IFavouriteState,
	IProductsCarouselData,
	{ rejectValue: string }
>(
	'favourite/fetchAddFavourite',
	async function (id, { rejectWithValue, getState }) {
		const productState = getState() as AppState
		const product: IProductsCarouselData | any =
			productState.favouriteReducer.products.find(
				(product: IProductsCarouselData) => product.id === id.id
			)

		const temp = { ...product }
		temp.choosen = !temp.choosen

		const responce = await axios.put(
			`https://63c325198bb1ca34755e00d7.mockapi.io/products/${product.id}`,
			temp
		)

		if (responce.statusText !== 'OK') {
			return rejectWithValue("Can't add product .Server Error!")
		}

		return (await responce.data) as IFavouriteState
	}
)

export const fetchAddFavouriteService = (
	state: IFavouriteState,
	action: AnyAction
) => {
	const productIndex = state.products.findIndex(
		el => el.id === action.payload.id
	)
	const newProducts = state.products.map((item, i) =>
		i === productIndex ? { ...item, choosen: !item.choosen } : item
	)

	state.products = newProducts

	if (state.products[productIndex].choosen) {
		toast.success(`${state.products[productIndex].title} added to favorites`, {
			position: 'bottom-left',
			theme: 'colored',
			autoClose: 1500
		})
	} else {
		toast.error(
			`${state.products[productIndex].title} removed from favorites`,
			{
				position: 'bottom-left',
				theme: 'colored',
				autoClose: 1500
			}
		)
	}
}
