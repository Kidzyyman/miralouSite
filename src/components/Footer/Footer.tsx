import { FC } from 'react'
import { Link } from 'react-router-dom'

import HeaderLogo from '@/assets/icons/header-icons/Logo.svg'

import facebookIcon from '../../assets/icons/footer-icons/facebook-icon.svg'
import googleIcon from '../../assets/icons/footer-icons/google-icon.svg'
import instagramIcon from '../../assets/icons/footer-icons/instagram-icon.svg'
import twitterIcon from '../../assets/icons/footer-icons/twitter-icon.svg'
import youtubeIcon from '../../assets/icons/footer-icons/youtube-icon.svg'

const Footer: FC = () => {
	return (
		<footer className='footer-section'>
			<div className='container-second'>
				<div className='footer-grid'>
					<div className='footer-grid__item'>
						<img src={HeaderLogo} className='header-logo' alt={HeaderLogo} />
						<p className='footer-grid__item__descr'>
							Sed perspiciatis unde omnis natus error voluptatem accusantium
							doloremque laudantium, totam rem aperiam.
						</p>
						<div className='footer-grid__item-icons'>
							<div className='footer-grid__item-icons__icon'>
								<div className='footer-grid__item-icons__icon_circle'>
									<img
										src={facebookIcon}
										alt='facebook-icon'
										className='footer-grid__item-icons__icon__img'
									/>
								</div>
							</div>
							<div className='footer-grid__item-icons__icon'>
								<div className='footer-grid__item-icons__icon_circle'>
									<img
										src={twitterIcon}
										alt='twitter-icon'
										className='footer-grid__item-icons__icon__img'
									/>
								</div>
							</div>
							<div className='footer-grid__item-icons__icon'>
								<div className='footer-grid__item-icons__icon_circle'>
									<img
										src={youtubeIcon}
										alt='youtube-icon'
										className='footer-grid__item-icons__icon__img'
									/>
								</div>
							</div>
							<div className='footer-grid__item-icons__icon'>
								<div className='footer-grid__item-icons__icon_circle'>
									<img
										src={instagramIcon}
										alt='instagram-icon'
										className='footer-grid__item-icons__icon__img'
									/>
								</div>
							</div>
							<div className='footer-grid__item-icons__icon'>
								<div className='footer-grid__item-icons__icon_circle'>
									<img
										src={googleIcon}
										alt='google-icon'
										className='footer-grid__item-icons__icon__img'
									/>
								</div>
							</div>
						</div>
					</div>
					<div className='footer-grid__item'>
						<h2 className='footer-grid__item__title'>Quick Link</h2>
						<Link to={'/aboutUs'} className='footer-grid__item__link'>
							About Us
						</Link>
						<Link to={'/home'} className='footer-grid__item__link'>
							Shop
						</Link>
						<Link to={'/products'} className='footer-grid__item__link'>
							Products
						</Link>
						<Link to={'/news'} className='footer-grid__item__link'>
							Blog
						</Link>
					</div>
					<div className='footer-grid__item'>
						<h2 className='footer-grid__item__title'>Category</h2>
						<Link to={''} className='footer-grid__item__link'>
							Indoor Plants
						</Link>
						<Link to={''} className='footer-grid__item__link'>
							Outdoor Plants
						</Link>
						<Link to={''} className='footer-grid__item__link'>
							Trending Plants
						</Link>
						<Link to={''} className='footer-grid__item__link'>
							Best Selling
						</Link>
					</div>
					<div className='footer-grid__item'>
						<h2 className='footer-grid__item__title'>Contact Us</h2>
						<Link to={''} className='footer-grid__item__link footer_email'>
							hello@nursery.io
						</Link>
						<Link to={''} className='footer-grid__item__link'>
							2563 High street,
						</Link>
						<Link to={''} className='footer-grid__item__link'>
							2563 High street,
						</Link>
					</div>
				</div>
			</div>
			<section className='footer-section-underground'>
				<div className='container-second'>
					<div className='footer-section-underground-main'>
						<p className='footer-section-underground-description'>
							<span>Copyright</span> © 2019 The Landix, All Right Reserved
							amentotech
						</p>
						<nav className='footer-section-undergorund-nav'>
							<ul className='footer-section-underground-nav-ul'>
								<li className='footer-section-undeground-nav-ul__li'>
									<Link to={''}>News</Link>
								</li>
								<li className='footer-section-undeground-nav-ul__li'>
									<Link to={''}>About</Link>
								</li>
								<li className='footer-section-undeground-nav-ul__li'>
									<Link to={''}>How it works</Link>
								</li>
								<li className='footer-section-undeground-nav-ul__li'>
									<Link to={''}>Privacy Policy</Link>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</section>
		</footer>
	)
}

export default Footer
