export const mockPlantData = [
	{
		id: 1,
		title: 'Watering',
		description: 'Free shipping around the world for all orders over $80',
		image: require('../../assets/plantCare/plant-care-1.svg').default
	},
	{
		id: 2,
		title: 'Light',
		description:
			'Secure Payment services is a safer, faster, more secure way to pay ',
		image: require('../../assets/plantCare/plant-care-2.svg').default
	},
	{
		id: 3,
		title: 'Fertilizer',
		description: 'Eco-Friendly product keeps both Invironmental and human safe',
		image: require('../../assets/plantCare/plant-care-3.svg').default
	}
]
