import { FC } from 'react'

import { IProductsCarousel } from '@/types/IProductCarousel/IProductCarousel'
import useWindowDimensions from '@/utils/headerResize'

import FavoriteIcon from './FavouriteIcon'

const ProductsItem: FC<IProductsCarousel> = ({
	item,
	addToCart,
	addToFavourite
}) => {
	const { width } = useWindowDimensions()

	return (
		<div className='products-item'>
			<div className='products-item__block1'>
				<img
					src={item.image}
					alt={item.title}
					className='products-item__block1__img'
				/>
			</div>
			<div className='products-item__block2'>
				<h2 className='products-item__block2__title'>{item.title}</h2>
				<h3 className='products-item__block2__subtitle'>{item.subtitle}</h3>
				<div className='products-item__block2__price'>${item.price}</div>

				<button
					onClick={addToCart}
					className={
						width > 998 ? 'products-item-btn' : 'products-item-btn_block'
					}
				>
					Add to Cart
				</button>
			</div>
			<FavoriteIcon addFavouriteItem={addToFavourite} favouriteItem={item} />
		</div>
	)
}
export default ProductsItem
