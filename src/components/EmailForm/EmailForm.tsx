import { FC } from 'react'

import { useEmail } from '../hooks/useEmail'
import ButtonSubmit from '../ui/buttons/ButtonSubmit'
import EmailFormTitle from './EmailFormTitle'

const EmailForm: FC = () => {
	const { form, sendEmail, onChanged, value } = useEmail()

	return (
		<div className='wrapper'>
			<section className='emailForm-section'>
				<div className='container-second'>
					<EmailFormTitle />
					<form ref={form} onSubmit={sendEmail} className='emailForm-form'>
						<input
							onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
								onChanged(e)
							}
							value={value}
							type='text'
							name='email'
							className='emailForm-input'
							placeholder='Your Email'
						/>
						<ButtonSubmit />
					</form>
				</div>
			</section>
		</div>
	)
}
export default EmailForm
