import { ILatestPosts } from '@/types/ILatestPosts/ILatestPosts'

export const latestList: ILatestPosts[] = [
	{
		id: '1',
		image: require('../../assets/latestPost/latest-post-1.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '2',
		image: require('../../assets/latestPost/latest-post-2.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '3',
		image: require('../../assets/latestPost/latest-post-3.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '4',
		image: require('../../assets/latestPost/latest-post-4.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '5',
		image: require('../../assets/latestPost/latest-post-1.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '6',
		image: require('../../assets/latestPost/latest-post-2.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '7',
		image: require('../../assets/latestPost/latest-post-3.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	},
	{
		id: '8',
		image: require('../../assets/latestPost/latest-post-4.png'),
		date: 'Jun 18, 2021',
		description: 'From banking and insurance to wealth'
	}
]
