export const plantTitles = [
	{
		title: 'Free Delivery',
		subtitle: 'Free shipping the world',
		img: require('@/assets/prePlantCollection/freeDelivery.svg').default
	},
	{
		title: 'Secure Payment',
		subtitle: 'Secure Payment services is safer',
		img: require('@/assets/prePlantCollection/secure.svg').default
	},
	{
		title: 'Eco Friendly',
		subtitle: 'Eco-Friendly product keeps',
		img: require('@/assets/prePlantCollection/eco.svg').default
	}
]
