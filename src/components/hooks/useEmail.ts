import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'

import emailjs from '@emailjs/browser'

export const useEmail = () => {
	const form = React.useRef() as React.MutableRefObject<HTMLFormElement>

	const [value, setValue] = useState<string>('')
	const [validateValue, setValidateValue] = useState(false)

	const onChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
		setValue(e.target.value)
	}

	useEffect(() => {
		//if validate do request else null
		const validateValueCheck = (value: string) => {
			value.split('').includes('@')
				? setValidateValue(true)
				: setValidateValue(false)
		}
		validateValueCheck(value)
	}, [value])

	const sendEmail = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()

		if (validateValue) {
			emailjs
				.sendForm(
					'service_v5ydo1h',
					'template_e74cxaf',
					form.current,
					'9-6UNyMFZczT-r5ZO'
				)
				.then(
					() => {
						toast.success('Your email is successful 🍃', {
							position: 'top-center',
							autoClose: 2000,
							hideProgressBar: true,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true,
							progress: undefined,
							theme: 'dark'
						})
					},
					error => {
						console.log(error)
					}
				)
		} else {
			toast.error('Your email is wrong 🍂', {
				position: 'top-center',
				autoClose: 2000,
				hideProgressBar: true,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				theme: 'dark'
			})
		}
		setValue('')
	}

	return {
		form,
		sendEmail,
		onChanged,
		value
	}
}
