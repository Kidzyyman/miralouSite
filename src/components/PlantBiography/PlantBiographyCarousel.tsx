import { FC } from 'react'

import { motion } from 'framer-motion'
import { EffectCoverflow, Pagination } from 'swiper'
import 'swiper/css'
import 'swiper/css/effect-coverflow'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

import { useTypedSelector } from '@/pages/Cart/Cart'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'
import { framerMotion } from '@/utils/FramerMotion'

import { useShownButton } from '../hooks/useShownButton'
import { MPlantBiographyItem } from './PlantBiographyItem'

const PlantBiographyCarousel: FC = () => {
	const { products } = useTypedSelector(state => state.favouriteReducer)

	const productsUniqueImg: string[] = Array.from(
		new Set(products.map((item: IProductsCarouselData) => item.image))
	)
	const { currentSlide, swiperRef } = useShownButton(2)

	return (
		<motion.div
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='plantBiography-section'
		>
			<Swiper
				effect={'coverflow'}
				grabCursor={true}
				centeredSlides={true}
				slidesPerView={'auto'}
				coverflowEffect={{
					rotate: 50,
					stretch: -100,
					depth: 100,
					modifier: 1,
					slideShadows: true
				}}
				initialSlide={2}
				modules={[EffectCoverflow, Pagination]}
				pagination={{ clickable: true }}
				className='mySwiper'
				ref={swiperRef}
			>
				{productsUniqueImg.map((image, i) => (
					<SwiperSlide key={i}>
						<MPlantBiographyItem
							custom={i + 1}
							variants={framerMotion.animationRightUpToLeft}
							image={image}
							i={i}
							currentSlide={currentSlide}
						/>
					</SwiperSlide>
				))}
			</Swiper>
		</motion.div>
	)
}

export default PlantBiographyCarousel
