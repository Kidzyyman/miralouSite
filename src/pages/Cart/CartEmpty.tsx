import { FC } from 'react'
import Lottie from 'react-lottie'

import BackLinkArrow from '@/components/ui/buttons/BackLinkArrow'
import useWindowDimensions from '@/utils/headerResize'

import cartEmpty from '../../assets/woman-reading-book-under-the-tree.json'

const CartEmpty: FC = () => {
	const { width } = useWindowDimensions()
	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: cartEmpty,
		renderSettings: {
			preserveAspectRatio: 'xMidYMid slice'
		}
	}
	return (
		<div className='cart-empty'>
			<Lottie
				options={defaultOptions}
				height={width > 998 ? 700 : width * 0.7}
				width={width > 998 ? 700 : width * 0.7}
			/>
			<p className='continue-shopping__link'>Your cart is currently empty</p>
			<BackLinkArrow title='Start shopping' />
		</div>
	)
}
export default CartEmpty
