import { TypedUseSelectorHook, useSelector } from 'react-redux'

import { RootState } from '@/store'

import CartEmpty from './CartEmpty'
import CartSummary from './CartSummary'

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector

const Cart = () => {
	const cart = useTypedSelector(state => state.cartReducer)

	return (
		<div className='cart-container'>
			<h2 className='cart-container__title'>Shopping Cart</h2>
			{cart.cartItems.length === 0 ? (
				<CartEmpty />
			) : (
				<CartSummary cart={cart} />
			)}
		</div>
	)
}

export default Cart
