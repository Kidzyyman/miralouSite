import { FC } from 'react'
import { useDispatch } from 'react-redux'

import Header from '@/components/Header/Header'
import { useSearchInput } from '@/components/hooks/useSearchInput'
import BackLinkArrow from '@/components/ui/buttons/BackLinkArrow'
import SearchInput from '@/components/ui/inputs/SearchInput'
import { fetchAddFavourite } from '@/store/favourite/services/FavouritesApi'
import { IFavourite } from '@/types/IFavourite/IFavourite'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'
import useWindowDimensions from '@/utils/headerResize'

import FavouriteItem from './FavouriteItem'
import FavouritesNoMatches from './FavouritesNoMatches'

const FavouriteSummary: FC<IFavourite> = ({ favourite }) => {
	const dispatch = useDispatch()
	const { width } = useWindowDimensions()

	const handleRemoveFavourites = (item: IProductsCarouselData): void => {
		dispatch(fetchAddFavourite(item))
	}
	const { searchInput, searchFavourites, handleSearch, matches } =
		useSearchInput()

	return (
		<>
			{width < 769 && <Header />}
			<div className='favourites-summary'>
				<div className='favourites-main-titleBlock'>
					<h1 className='favourites-summary-main-title'>Favourites</h1>
					<div className='favourites-arrow_postion'>
						<BackLinkArrow />
					</div>
				</div>
				<SearchInput value={searchInput} search={searchFavourites} />
				<div className='favourites-summary-mainblock'>
					{handleSearch(favourite).map((item: IProductsCarouselData) => (
						<FavouriteItem
							key={item.id}
							item={item}
							handleRemoveFavourites={handleRemoveFavourites}
						/>
					))}
					{!matches && <FavouritesNoMatches />}
				</div>
			</div>
		</>
	)
}
export default FavouriteSummary
