import { FC } from 'react'
import Lottie from 'react-lottie'
import { Link } from 'react-router-dom'

import plantEmpty from '@/assets/plantEmpty.json'
import useWindowDimensions from '@/utils/headerResize'

const FavouriteEmpty: FC = () => {
	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: plantEmpty,
		renderSettings: {
			preserveAspectRatio: 'xMidYMid slice'
		}
	}

	const { width } = useWindowDimensions()

	return (
		<section className='favourites-empty'>
			<Lottie
				options={defaultOptions}
				height={400}
				width={width < 569 ? width - 400 + 300 : 400}
			/>
			<h2 className='favourites-summary-block__title'>
				You don't have any favorites
			</h2>
			<div className='favourites-empty-descr'>
				<Link to={'/home'}>
					<div className='favourites-empty-descr__link'>
						<svg
							xmlns='http://www.w3.org/2000/svg'
							width='20'
							height='20'
							fill='currentColor'
							className='bi bi-arrow-left'
							viewBox='0 0 16 16'
						>
							<path
								fillRule='evenodd'
								d='M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z'
							/>
						</svg>
						<h2>Go back to the main page</h2>
					</div>
				</Link>
			</div>
		</section>
	)
}
export default FavouriteEmpty
