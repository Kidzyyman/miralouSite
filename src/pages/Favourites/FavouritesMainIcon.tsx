import { FC } from 'react'
import { Link } from 'react-router-dom'

import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useTypedSelector } from '@/components/Header/CartIcon'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

const FavouritesMainIcon: FC = () => {
	const { products } = useTypedSelector(state => state.favouriteReducer)

	const totalAmountFavourites: number = products.filter(
		(item: IProductsCarouselData) => item.choosen
	).length

	return (
		<div className='favouriteMainIcon-block'>
			<div className='favouriteMainIcon-block-icon'>
				<Link to={'/favourites'}>
					<FontAwesomeIcon
						icon={faHeart}
						className={'favouritesIcon-large fa-beat'}
					/>
					<span className='favouriteMainIcon-block-icon__count'>
						{totalAmountFavourites}
					</span>
				</Link>
			</div>
			<h4 className='favouriteMainIcon-block__title'>favourites </h4>
		</div>
	)
}
export default FavouritesMainIcon
